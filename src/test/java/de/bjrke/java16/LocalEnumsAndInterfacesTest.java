package de.bjrke.java16;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class LocalEnumsAndInterfacesTest {

    @Test
    void possibleSinceJava8() {

        class LocalObject {

            int one() {
                return 1;
            }
        }

        assertEquals(1, new LocalObject().one());
    }

    @Test
    void localEnumTest() {
        enum LocalEnum {
            FOO
        }
        assertEquals("FOO", LocalEnum.FOO.name());
    }

    @Test
    void localInterface() {
        interface LocalInterface {

            int doIt();
        }

        final LocalInterface lambda = () -> 1;

        assertEquals(1, lambda.doIt());
    }
}
