package de.bjrke.java16;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Objects;
import org.junit.jupiter.api.Test;

public class PatternTest {

    @Test
    void instanceOfWithCast() {
        Object o = "hello";
        if (o instanceof String) {
            assertEquals("HELLO", ((String) o).toUpperCase());
        }
    }

    @Test
    void instanceOfWithoutCast() {
        Object o = "hello";
        if (o instanceof String s) {
            assertEquals("HELLO", s.toUpperCase());
        }
    }

    class EasyEquals {

        String field1;
        String field2;

        public boolean oldEquals(Object o) {
            return this == o
                    || o instanceof EasyEquals
                            && Objects.equals(field1, ((EasyEquals) o).field1)
                            && Objects.equals(field2, ((EasyEquals) o).field2);
        }

        @Override
        public boolean equals(Object o) {
            return this == o
                    || o instanceof EasyEquals t
                            && Objects.equals(field1, t.field1)
                            && Objects.equals(field2, t.field2);
        }

        @Override
        public int hashCode() {
            return Objects.hash(field1, field2);
        }
    }
}
