package de.bjrke.java16;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;

class StreamTest {

    @Test
    void toListTest() {
        assertEquals(List.of(1, 2, 3, 4), Stream.of(1, 2, 3, 4).toList());
    }

    @Test
    void mapMulti() {
        final var barbaz =
                Stream.of("foo", "bar", "baz")
                        .<String>mapMulti(
                                (s, consumer) -> {
                                    if (s.startsWith("b")) {
                                        for (var ch : s.toCharArray()) {
                                            consumer.accept(String.valueOf(ch));
                                        }
                                    }
                                })
                        .collect(Collectors.joining());
        assertEquals("barbaz", barbaz);
    }
}
