package de.bjrke.java16;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class RecordTest {

    private static final double DELTA = 0.0001;

    private interface Shape {

        double getArea();

        double getPerimeter();
    }

    private static final class Circle implements Shape {

        public final double radius;

        public Circle(double radius) {
            this.radius = radius;
        }

        @Override
        public double getArea() {
            return Math.PI * radius * radius;
        }

        @Override
        public double getPerimeter() {
            return 2 * Math.PI * radius;
        }
    }

    private record Rectangle(double width, double height) implements Shape {

        @Override
        public double getArea() {
            return width * height;
        }

        @Override
        public double getPerimeter() {
            return 2 * (width + height);
        }
    }

    @Test
    void rectangle() {
        var rect = new Rectangle(10, 10);
        assertEquals(100, rect.getArea(), DELTA);
        assertEquals(40, rect.getPerimeter(), DELTA);
        assertEquals(10, rect.height(), DELTA);
        assertEquals("Rectangle[width=10.0, height=10.0]", rect.toString());
    }

    @Test
    void circle() {
        var rect = new Circle(1);
        assertEquals(Math.PI, rect.getArea(), DELTA);
        assertEquals(2 * Math.PI, rect.getPerimeter(), DELTA);
    }
}
