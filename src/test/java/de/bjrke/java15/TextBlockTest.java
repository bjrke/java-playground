package de.bjrke.java15;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class TextBlockTest {

    @Test
    void shouldIgnoreIndentation() {
        assertEquals(
                "line 1\n line 2\n  line 3\n   line 4\n    line 5\n",
                """
                line 1
                 line 2
                  line 3
                   line 4
                    line 5
                """);
    }

    @Test
    void stringFormat() {
        assertEquals(
                "foo bar baz\n",
                """
                foo %s baz
                """.formatted("bar"));
    }
}
