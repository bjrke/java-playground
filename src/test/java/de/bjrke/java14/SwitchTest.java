package de.bjrke.java14;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

class SwitchTest {

    enum AnEnum {
        FOO(1),
        BAR(2),
        ;

        private final int i;

        AnEnum(int i) {
            this.i = i;
        }
    }

    @ParameterizedTest
    @EnumSource(AnEnum.class)
    void classicSwitch(final AnEnum anEnum) {
        final int actual;
        switch (anEnum) {
            case FOO:
                actual = 1;
                break;
            case BAR:
                actual = 2;
                break;
            default:
                throw new UnsupportedOperationException("Unexpected value: " + anEnum);
        }
        assertEquals(anEnum.i, actual);
    }

    @ParameterizedTest
    @EnumSource(AnEnum.class)
    void classicSwitchWithExtractedMethod(final AnEnum anEnum) {
        final int actual = extractedSwitch(anEnum);
        assertEquals(anEnum.i, actual);
    }

    private int extractedSwitch(AnEnum anEnum) {
        switch (anEnum) {
            case FOO:
                return 1;
            case BAR:
                return 2;
        }
        throw new UnsupportedOperationException("Unexpected value: " + anEnum);
    }

    @ParameterizedTest
    @EnumSource(AnEnum.class)
    void java14Switch(final AnEnum anEnum) {
        final int actual =
                switch (anEnum) {
                    case FOO -> 1;
                    case BAR -> 2;
                };
        assertEquals(anEnum.i, actual);
    }
}
