package de.bjrke.java14;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class HelpfulNullPointerExceptionTest {

    @Test
    void getAStackTrace() {
        try {
            Object foo = null;
            foo.toString();
        } catch (NullPointerException e) {
            e.printStackTrace();
            assertEquals(
                    "Cannot invoke \"Object.toString()\" because \"foo\" is null", e.getMessage());
        }
    }
}
