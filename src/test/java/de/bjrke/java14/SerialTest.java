package de.bjrke.java14;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serial;
import java.io.Serializable;

public class SerialTest {

    private static class SerializableObject implements Serializable {

        private int aField;

        @Serial
        private void readObject(final ObjectInputStream in)
                throws IOException, ClassNotFoundException {
            in.defaultReadObject();
        }

        /** gives a warning */
        @Serial
        private void writeObject(/* final ObjectOutputStream out */ ) {}

        @Override
        public String toString() {
            return "SerializableObject{aField=%d}".formatted(aField);
        }
    }
}
