package de.bjrke.java17;

public class SealedTest {

    sealed interface Shape {
        record Rectangle(double width, double height) implements Shape {}

        record Circle(double radius) implements Shape {}
    }

    // base class, can also be abstract or an interface, must not be static
    static sealed class SealedBase {}

    // subclasses need to be final
    private final class FinalSubClass extends SealedBase {}

    // or subclasses need to be sealed itself
    static sealed class SealedSubClass extends SealedBase permits ExternalSealedSub {}
}
