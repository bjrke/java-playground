package de.bjrke.java17;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HexFormat;
import org.junit.jupiter.api.Test;

public class HexFormatTest {

    @Test
    void fromHexDigits() {
        assertEquals(0x7f, HexFormat.fromHexDigits("7f"));
    }

    @Test
    void toHexDigits() {
        assertEquals("00000400", HexFormat.of().toHexDigits(1024));
    }
}
