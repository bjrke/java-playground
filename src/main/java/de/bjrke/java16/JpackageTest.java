package de.bjrke.java16;

import java.util.Arrays;

/**
 * jpackage --type app-image -i build/libs -n test --main-class de.bjrke.java16.JpackageTest
 * --main-jar java-playground-1.0-SNAPSHOT.jar
 */
public class JpackageTest {

    public static void main(String[] args) {
        System.out.println("Hello World!\n" + Arrays.toString(args));
    }
}
