buildscript { repositories { mavenCentral() } }

plugins {
    java
    // https://mvnrepository.com/artifact/com.diffplug.spotless/spotless-plugin-gradle
    id("com.diffplug.spotless") version "6.0.5"
}

group = "de.bjrke"

version = "1.0-SNAPSHOT"

java.sourceCompatibility = JavaVersion.VERSION_17

java.targetCompatibility = JavaVersion.VERSION_17

repositories { mavenCentral() }

dependencies {
    // https://mvnrepository.com/artifact/org.junit.jupiter/junit-jupiter
    testImplementation("org.junit.jupiter", "junit-jupiter", "5.8.2")
}

tasks.getByName<Test>("test") { useJUnitPlatform() }

spotless {
    kotlinGradle { ktfmt().kotlinlangStyle() }
    java { googleJavaFormat().aosp() }
}
